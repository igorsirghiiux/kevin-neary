import React from "react";
import { Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
import Speaking from "./pages/Speaking";
import InfluencerPrograms from "./pages/InfluencerPrograms";
import About from "./pages/About";
import EventPlanner from "./pages/EventPlanner";
import Contact from "./pages/Contact";
import Books from "./pages/Books";
const App = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/speaking" element={<Speaking />} />
        <Route path="/influencer-programs" element={<InfluencerPrograms />} />
        <Route path="/about" element={<About />} />
        <Route path="/books" element={<Books />} />
        <Route path="/event-planner" element={<EventPlanner/>} />
        <Route path="/contact" element={<Contact/>} />
      </Routes>
    </div>
  );
};

export default App;
