import Footer from "../components/footer/Footer";
import SubscribeForm from "../components/Forms/subscribeform/SubscribeForm";
import EventPlanners from "../components/layouts/eventplanners/herosection/EventPlanners";
import Navbar from "../components/navbar/Navbar";

const EventPlanner = () => {
  return (
    <section>
      <Navbar />
      <EventPlanners />
      <SubscribeForm/>
      <Footer />
    </section>
  );
};

export default EventPlanner;
