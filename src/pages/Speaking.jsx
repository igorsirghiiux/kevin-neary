import SpeakingHeroSection from "../components/layouts/speaking/herosection/SpeakingHeroSection";
import SpeakingContent1 from "../components/layouts/speaking/speakingcontent1/SpeakingContent1";
import SpeakingContent2 from "../components/layouts/speaking/speakingcontent2/SpeakingContent2";

import SubscribeForm from "../components/Forms/subscribeform/SubscribeForm";
import Footer from "../components/footer/Footer";
import Navbar from "../components/navbar/Navbar";

const Speaking = () => {
  return (
    <section>
      <Navbar />
      <SpeakingHeroSection />
      <SpeakingContent1 />
      <SpeakingContent2 />

      <SubscribeForm />
      <Footer />
    </section>
  );
};

export default Speaking;
