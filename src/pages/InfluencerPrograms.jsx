import React from "react";
import Navbar from "../components/navbar/Navbar";
import InfluencerHeroSection from "../components/layouts/influencerprograms/herosection/InfluencerHeroSection";
import InfluenceContent from "../components/layouts/influencerprograms/influencecontent/InfluenceContent";
import InfluenceContent2 from "../components/layouts/influencerprograms/influencecontent2/InfluenceContent2";
import SubscribeForm from "../components/Forms/subscribeform/SubscribeForm";
import Footer from "../components/footer/Footer";

const InfluencerPrograms = () => {
  return (
    <section>
      <Navbar />
      <InfluencerHeroSection />
      <InfluenceContent />
      <InfluenceContent2 />
      <SubscribeForm />
      <Footer />
    </section>
  );
};

export default InfluencerPrograms;
