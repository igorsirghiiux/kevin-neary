import React from "react";
import HeroSection from "../components/layouts/homepage/herosection/HeroSection";
import Navbar from "../components/navbar/Navbar";
import Content1 from "../components/layouts/homepage/homecontent1/Content1";
import SlideCards from "../components/slidevideocom/SlideCards";
import Content2 from "../components/layouts/homepage/homecontent2/Content2";
import Content3 from "../components/layouts/homepage/homecontent3/Content3";
import Content4 from "../components/layouts/homepage/homecontent4/Content4";
import SubscribeForm from "../components/forms/subscribeform/SubscribeForm";
import Footer from "../components/footer/Footer";

function HomePage() {
  return (
    <section>
      <Navbar />
      <HeroSection />
      <Content1 />
      <SlideCards />
      <Content2 />
      <Content3 />
      <Content4 />
      <SubscribeForm />
      <Footer />
    </section>
  );
}

export default HomePage;
