import React from 'react'
import Footer from '../components/footer/Footer'
import SubscribeForm from '../components/Forms/subscribeform/SubscribeForm'
import BooksPage from '../components/layouts/books/BooksPage'
import Navbar from '../components/navbar/Navbar'


const Books = () => {
  return (
    <section>
        <Navbar />
        <BooksPage />
        <SubscribeForm />
        <Footer />
    </section>
  )
}

export default Books