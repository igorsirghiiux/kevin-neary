import Footer from "../components/footer/Footer";
import ContactPage from "../components/layouts/contact/ContactPage";
import Navbar from "../components/navbar/Navbar";

const Contact = () => {
  return (
    <section>
        <Navbar />
      <ContactPage />
      <Footer />
    </section>
  );
};

export default Contact;
