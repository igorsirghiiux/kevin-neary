import AboutContent from "../components/layouts/about/aboutcontent/AboutContent"
import AboutContent2 from "../components/layouts/about/aboutcontent2/AboutContent2"
import AboutHeroSection from "../components/layouts/about/herosection/AboutHeroSection"
import Navbar from "../components/navbar/Navbar"
import SubscribeForm from "../components/Forms/subscribeform/SubscribeForm";
import Footer from "../components/footer/Footer";


const About = () => {
  return (
    <section>
      <Navbar />
      <AboutHeroSection />
      <AboutContent />
      <AboutContent2 />
      <SubscribeForm />
      <Footer />
    </section>
  );
}

export default About