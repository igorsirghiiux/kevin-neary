import React, { useEffect, useState } from "react";
import "./emailformjs.styles.css";
import emailjs from "@emailjs/browser";
import { FaArrowAltCircleLeft } from "react-icons/fa";

const EmailForm = () => {
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    company: "",
    location: "",
    eventDate: "",
    referral: "",
    message: "",
  });
  const [renderAlert, setRenderAlert] = useState(false);

  useEffect(() => {
    let timeout;
    if (renderAlert) {
      timeout = setTimeout(() => {
        setRenderAlert(false);
      }, 3000);
    }
    return () => clearTimeout(timeout);
  }, [renderAlert]);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    emailjs
      .send(
        "service_fv09tzq",
        "template_jzv6dlj",
        formData,
        "0IJEziQodmux_QxMu"
      )
      .then(
        (response) => {
          console.log("SUCCESS!", response);
          setRenderAlert(true);
        },
        (error) => {
          console.log("FAILED...", error);
        }
      );
  };

  return (
    <>
      <form onSubmit={handleSubmit} className="contact-form">
        <div>{renderAlert && <p>Your Message Submitted Successfully</p>}</div>

        <div className="flex-input">
          <label className="space">
            First Name*
            <input
              type="text"
              name="firstName"
              value={formData.firstName}
              onChange={handleChange}
              required
            />
          </label>
          <label>
            Last Name*
            <input
              type="text"
              name="lastName"
              value={formData.lastName}
              onChange={handleChange}
              required
            />
          </label>
        </div>
        <div className="flex-input">
          <label>
            E-mail*
            <input
              type="email"
              name="email"
              value={formData.email}
              onChange={handleChange}
              required
            />
          </label>
          <label>
            Phone Number
            <input
              type="tel"
              name="phone"
              value={formData.phone}
              onChange={handleChange}
            />
          </label>
        </div>

        <label>
          Company / Organization / Event*
          <input
            type="text"
            name="company"
            value={formData.company}
            onChange={handleChange}
            required
          />
        </label>
        <div className="flex-input">
          <label>
            Event Location
            <input
              type="text"
              name="location"
              value={formData.location}
              onChange={handleChange}
            />
          </label>
          <label>
            Event Date (if known)
            <input
              className="date-resize"
              type="date"
              name="eventDate"
              value={formData.eventDate}
              onChange={handleChange}
            />
          </label>
        </div>

        <label>
          How did you hear about us?
          <input
            type="text"
            name="referral"
            value={formData.referral}
            onChange={handleChange}
          />
        </label>
        <label>
          How can we make your event amazing, or is there something else we can
          help you with?*
          <textarea
            name="message"
            value={formData.message}
            onChange={handleChange}
            required
          />
        </label>
        <div className="button-form-center">
          <button className="form-btn" type="submit">
            Send{" "}
            <span className="icon-btn">
              <FaArrowAltCircleLeft color="#000" fontSize="2rem" />
            </span>
          </button>
        </div>
      </form>
    </>
  );
};

export default EmailForm;
