import React from "react";
import "./subscribeform.styles.css";

const SubscribeForm = () => {
  return (
    <div className="wrap-article-content-subscribe">
      <div className="top-image"></div>
      <div className="wrap-article-subscribe">
        <div className="article-container">
          <div className="column-article-subscribe">
            <div className="column1-subscribe">
              <h5>
                Get "The Baer Facts" newsletter: twice monthly marketing &
                customer experience tips, stats, trivia, and fun
              </h5>
            </div>
            <div className="column2-subscribe">
              <div className="form-container-subscribe">
                <form action="focus" className="wrap-input">
                  <input type="text" placeholder="Your Name" />
                  <input type="email" placeholder="Your E-mail" />
                  <a
                    className="btn-secondary-subscribe"
                    href="https://www.jaybaer.com/about-jay/"
                    target=""
                  >
                    Subscribe
                  </a>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SubscribeForm;
