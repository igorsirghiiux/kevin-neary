import React from "react";
import "./content.styles.css";
import AmericanExpress from "../../../../assets/AmericanExpress.png";
import AnyTime from "../../../../assets/antyime.png";
import Circle from "../../../../assets/Circle.png";
import Cisco from "../../../../assets/Cisco.png";
import clevland from "../../../../assets/clevland.png";
import hilton from "../../../../assets/hilton.png";
import Ibm from "../../../../assets/Ibm.png";
import jdpower from "../../../../assets/jdpower.png";
import mavoclinic from "../../../../assets/mavoclinic.png";
import novartis from "../../../../assets/novartis.png";
import pcma from "../../../../assets/pcma.png";
import Prudential from "../../../../assets/Prudential.png";
import purina from "../../../../assets/purina.png";
import sxsw from "../../../../assets/sxsw.png";
import VideoModal from "../../../videomodal/VideoModal";

const Content = () => {
  return (
    <div className="content-container">
      <div className="content-wrapper">
        
        <p className="logos-title">These companies love working with Jay</p>
        <div className="logos-gallery">
          <div className="logos-img-wrapper">
            <img width="91" height="51" src={Cisco} alt="cisco" />
          </div>
          <div className="logos-img-wrapper">
            <img width="168" height="43" src={Prudential} alt="Prudential" />
          </div>
          <div className="logos-img-wrapper">
            <img width="80" height="35" src={Ibm} alt="Ibm" />
          </div>
          <div className="logos-img-wrapper">
            <img width="51" height="43" src={Circle} alt="Circle" />
          </div>
          <div className="logos-img-wrapper">
            <img width="134" height="22" src={novartis} alt="novartis" />
          </div>
          <div className="logos-img-wrapper">
            <img width="99" height="26" src={sxsw} alt="sxsw" />
          </div>
          <div className="logos-img-wrapper">
            <img width="74" height="53" src={hilton} alt="hilton" />
          </div>
          <div className="logos-img-wrapper">
            <img width="54" height="59" src={mavoclinic} alt="mavoclinic" />
          </div>
          <div className="logos-img-wrapper">
            <img width="96" height="27" src={AmericanExpress} />
          </div>
          <div className="logos-img-wrapper">
            <img width="53" height="48" src={pcma} />
          </div>
          <div className="logos-img-wrapper">
            <img width="54" height="53" src={purina} />
          </div>
          <div className="logos-img-wrapper">
            <img width="145" height="30" src={AnyTime} />
          </div>
          <div className="logos-img-wrapper">
            <img width="93" height="44" src={clevland} />
          </div>
          <div className="logos-img-wrapper">
            <img width="111" height="17" src={jdpower} />
          </div>
        </div>
        <h4 className="article-title">
          Jay Baer grows businesses. A 7th-generation entrepreneur, he’s written
          6 best-selling books on customer acquisition and retention, and
          founded 5 multi-million dollar companies from scratch. More than 700
          brands – including 40 of the FORTUNE 500 – have relied on Jay to help
          them delight their customers and reach new levels of success.
        </h4>
      </div>
      <div className="content-bg-orange">
        <div className="top-image"></div>
        <div className="content-wrapper">

        <VideoModal />
        </div>
      </div>
      <div className="wrap-article-content1">
        <div className="wrap-article">
          <div className="article-container">
            <div className="column-article1">
              <div className="column1-content">
                <h2>
                  In-person and Virtual Programs That Inspire and Motivate Your
                  Audience
                </h2>
              </div>

              <div className="column2-content">
                <p>
                  Jay combines his experience as an entrepreneur, and as a
                  global business strategist working with the world’s most
                  iconic brands. His keynote programs that are the perfect blend
                  of practical, real-world guidance and inspirational, big
                  thinking. His objective is simple: help you grow your business
                  by gaining new customers (and keeping with ones you’ve already
                  earned) with eye-opening frameworks your competitors don’t
                  understand.
                </p>
                <p>
                  <strong>Recent accolades include:</strong>
                </p>
                <ul>
                  <li>Global Guru in Customer Experience</li>
                  <li>Global Guru in Internet Marketing</li>
                  <li>Top 10 Digital Marketing Experts in the World</li>
                  <li>Top Customer Experience Speakers</li>
                  <li>Top Customer Service Speakers</li>
                  <li>Top B2B Marketing Influencers</li>
                  <li>Top Content Marketers to Follow Right Now</li>
                  <li>Top 10 Customer Experience Influencers to Watch</li>
                  <li>Top Customer Experience Leaders You Must Follow</li>
                  <li>Top Content Marketing Influencers to Follow</li>
                </ul>

                <a
                  className="btn-secondary"
                  href="https://www.jaybaer.com/keynotes/"
                  target=""
                >
                  SEE ALL POPULAR PROGRAMS
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Content;
