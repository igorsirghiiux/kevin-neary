import React from "react";
import "./homecontent4.styles.css";
import AboutJay from "../../../../assets/aboutjay.png";

const Content4 = () => {
  return (
    <>
      <div className="wrap-article-content4">
        <div className="wrap-article">
          <div className="article-container">
            <div className="column-article-content4">
              <div className="column1-content4">
                <img src={AboutJay} alt="About" />
              </div>
              <div className="column2-content4">
                <h2>
                  About <span>Jay</span>
                </h2>
                <h5>
                  Jay Baer customizes his program to fit the precise needs of
                  your audience.
                </h5>
                <p>
                  Jay uses his experience as an entrepreneur and as an advisor
                  to major brands to research your industry and bring real-time,
                  spot-on recommendations that are hyper-relevant to your
                  attendees.
                </p>
                <a
                  className="btn-secondary-content4"
                  href="https://www.jaybaer.com/about-jay/"
                  target=""
                >
                  More About Jay
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Content4;
