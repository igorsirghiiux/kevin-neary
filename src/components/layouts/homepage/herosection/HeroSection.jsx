import React from "react";
import "./herosection.styles.css";
import Bgvideo from "../../../../assets/Bgvideo.mp4";

const HeroSection = () => {
  return (
    <section className="hero-section">
      <div className="overlay"></div>
      <video src={Bgvideo} autoPlay loop muted />
      <div className="container">
        <div className="content">
          <h2 className="hero-title">
            Global Speaker Inspiring Marketers into Action with Powerful
            Storytelling
          </h2>
          <h3>The Future of Marketing</h3>
          <p className="hero-paragraph">
            <br />
            
            " Fantastic insights Kevin at a brilliant event. You really helped
            me think differently about how we deliver our brand message. Thank
            you! "
            <br />
            <br />
            For your in-person or virtual event Kevin Neary will give your
            audience a memorable experience.
          </p>
          <div className="links-btn">
            <a className="btn-primary" href="#">
              Check kevin's Availability
            </a>
            <a className="btn-primary1" href="https://youtu.be/MG1xQRJY3CE">
              SEE Kevin IN ACTION!
            </a>
            {/* <a className="play-link" href="https://youtu.be/MG1xQRJY3CE">
              SEE JAY IN ACTION!
            </a> */}
          </div>
        </div>
      </div>
    </section>
  );
};

export default HeroSection;
