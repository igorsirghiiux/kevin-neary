import React from "react";
import "./homecontent3.style.css";
import SlideDataContent3 from "./slideDataContent3";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/pagination";
import "swiper/css/navigation";

const Homecontent3 = () => {
  return (
    <div className="wrap-article-content black-bg">
      <div className="bg-slide-fullwidth">
        <div className="glide-slide-container-center">
          <Swiper
            style={{
              "--swiper-navigation-size": "30px",
            }}
            slidesPerView={1}
            spaceBetween={80}
            loop={true}
            loopedSlides={2}
            navigation={true}
            modules={[Navigation]}
            className="slide-single-card-review"
          >
            {SlideDataContent3.map(({ img, logo, review, companyName },index) => (
              <SwiperSlide className="single-card-slide" key={index}>
                <div className="slide-display-flex">
                  <img src={img} alt="person" />
                </div>

                <div className="slide-content-review">
                  <h5>{review}</h5>
                  <div className="company-review-wrap">
                    <p>{companyName}</p>
                    <div className="logo-company-review">
                      <img src={logo} alt="logo" />
                    </div>
                  </div>
                </div>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
        <div className="wrap-article3 ">
          <div className="space-between-text">
            <h3>
              Check Jay's <span className="orange-text">Availability</span> for
              Your Event
            </h3>
            <a className="btn-primary article3" href="#">
              CONTACT JAY BAER
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Homecontent3;
