import pcma from "../../../../assets/person2-rw.png";
import Person2 from "../../../../assets/person2.png";
import SPR from "../../../../assets/person1-rw.jpg";
import Person1 from "../../../../assets/person1.png";
import Speak from "../../../../assets/person3-rw.png";
import Person3 from "../../../../assets/person3.png";

const SlideDataContent3 = [
  {
    img: Person1,
    logo: SPR,
    review: "Grow Your Business by Exceeding Your Customers’ Need for Speed",
    companyName: "Learn More and See Sample Video",
  },
  {
    img: Person2,
    logo: pcma,
    review: "Jay is an amazing speaker. He did an AWESOME job!",
    companyName: "Allayne Brackbill, Marketing Manager, S.P. Richards",
  },
  {
    img: Person3,
    logo: Speak,
    review:
      "We run a speakers bureau and work with hundreds of first rate speakers, and our experience working the Jay re-set the bar. As our opening keynote, Jay set the tone for a robust lineup of speakers for the next 3 days. His fast paced delivery and many, relevant, actionable and timely takeaways had the ‘chat’ on our platform on fire! As our event host, Jay added a level of polish and professionalism that truly took our event to the next level. ",
    companyName: "Katrina Mitchell, Speak!",
  },
];

export default SlideDataContent3;
