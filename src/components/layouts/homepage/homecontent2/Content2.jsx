import React from "react";
import "./content2.styles.css";
import Pickplaid from "../../../../assets/pickplaid.png";

const Content2 = () => {
  return (
    <div className="wrap-article-content1">
      <div className="bottom-image"></div>
      <div className="wrap-article">
        <div className="article-container">
          <div className="column-article">
            <div className="column1-content2">
              <h2>Pick Jay Baer and #PickPlaid</h2>
              <p>
                In addition to being the world’s most inspirational marketing
                and customer service keynote speaker, Jay Baer is also a unique
                visual presence on stage. Audiences are amazed by his array of
                very bold, very plaid suits. Plus, YOU get to pick the suit –
                even for virtual events! For each event, meeting planners are
                provided a link to a special, interactive tool where they select
                the color and pattern of suit Jay wears on stage. Want to match
                your corporate color? You can do it in seconds with the
                exclusive Dress Jay Baer app, just one of the many reasons
                meeting planners and company executives #PickPlaid.
              </p>
              <a
                className="btn-secondary"
                href="https://www.jaybaer.com/keynotes/"
                target=""
              >
                SEE ALL POPULAR PROGRAMS
              </a>
            </div>

            <div className="column2-content2">
              <img src={Pickplaid} alt="Pickplaid" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Content2;
