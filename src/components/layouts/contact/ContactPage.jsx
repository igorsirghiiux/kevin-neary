import "./contact.styles.css";
import ContactJay from "../../../assets/about-jay-hero.jpg";
import EmailForm from "../../forms/emailform/EmailForm";

const ContactPage = () => {
  return (
    <>
      <div className="hero-section">
        <div className="overlay"></div>
        <img className="speaking-bg " src={ContactJay} alt="about" />
      </div>
      <div className="container contact-container">
        <div className="content contact-position">
          <div className="contact-info">
            <h4>Contact</h4>
            <p>
              Thanks for your interest. Inquire here and we’ll be in touch
              immediately, or call Michelle Joyce now at (704) 965-2339.
            </p>
          </div>
          <EmailForm />
        </div>
      </div>
    </>
  );
};

export default ContactPage;
