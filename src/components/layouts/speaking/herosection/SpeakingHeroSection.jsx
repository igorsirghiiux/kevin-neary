import SpeakingBg from "../../../../assets/SpeakingBg.jpg";
import "./speakingherosection.styles.css";

const SpeakingHeroSection = () => {
  return (
    <>
      <div className="hero-section">
        <div className="overlay"></div>
        <img className="speaking-bg" src={SpeakingBg} alt="herosection" />
        <div className="container">
          <div className="content">
            <h2 className="hero-title">Keynotes</h2>
            <p className="hero-paragraph">
              Jay’s Most-Requested Live and Virtual Programs for 2023. Note that
              Jay is also a certified tequila sommelier, and can combine
              customer experience/marketing content with in-person or virtual
              tequila education and live tastings.
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default SpeakingHeroSection;
