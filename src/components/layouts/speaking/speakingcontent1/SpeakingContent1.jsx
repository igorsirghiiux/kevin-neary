import "./speakingcontent1.styles.css";

const SpeakingContent1 = () => {
  return (
    <div className="content-container-speaking">
      <div className="content-wrapper-speaking">
        <div className="article-container-speaking">
          <div className="column-article-speaking">
            <div className="column1-speaking">
              <h2>
                <a href="https://www.jaybaer.com/keynotes/the-time-to-win-grow-your-business-by-exceeding-customers-need-for-speed/">
                  The Time to Win: Grow Your Business by Exceeding Customers’
                  Need for Speed
                </a>
              </h2>
            </div>

            <div className="column2-speaking">
              <p>
                How much are you willing to wait? If you’re like most of your
                customers, the answer is very little.
              </p>
             
              <p>
                Your customers are deciding to buy from you today (and every
                day) based on how fast you are (or aren’t). More than half of
                all customers have made a recent purchase from a business that
                responded first, even if they were more expensive. And, 2/3 of
                all customers say speed is as important as price.
              </p>
              <a className="btn-primary speaking" href="#">
                Learn More About The Time To Win
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="line-speaking"></div>
    </div>
  );
};

export default SpeakingContent1;
