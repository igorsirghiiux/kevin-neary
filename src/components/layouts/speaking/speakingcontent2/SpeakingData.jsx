const SpeakingData = [
  {
    id: 1,
    title:
      "Talk Triggers: Turn Your Customers Into Your Ultimate Sales and Marketing Advantage",
    description:
      "The best way to grow ANY business is for your customers to grow it for you. But that only occurs if you deliver a customer experience that creates conversations.",
    description2:
      "Word of mouth influences 50% of all purchases, but we too often take this for granted. We just assume that our customers will talk about us. But they won’t, unless you give them a story to tell.",
    description3: "Jay teaches you how.",
    link: "",
    btn: "Learn More About Talk Triggers",
  },

  {
    id: 2,
    title:
      "Coveted Customer Experience: Grow Your Business by Focusing on 3 Things Your Customers Truly Care About",
    description:
      "You’ve heard it before. Over and over, in fact. “Improve your customer service.” “Optimize the customer experience.” But what does that even mean?",
    description2:
      "Today, when your intersection points with your customers may number in the dozens (or even hundreds) tackling customer service or customer experience holistically is impossible.",
    description3:
      "You can’t magically get better at every customer touch point. But you CAN get better at the touch points that matter.",
    link: "",
    btn: "Learn About Coveted Customer Experience",
  },

  {
    id: 3,
    title: "3 Drawbridges: How to Cross Your Customers’ Moat of Attention",
    description:
      "Total marketing messages increased 40% last year. To protect their attention, audiences’ are stashing it away inside a castle.",
    description2:
      "You cannot breach this castle by creating even MORE messages. But you can exceed your communication goals IF you convince your audiences to lower a drawbridge and invite you in.",
    description3:
      "Learn the 3 drawbridges in this fast, funny presentation filled with real-world examples!",
    link: "",
    btn: "Learn More About The 3 Drawbridges",
  },

  {
    id: 4,
    title: "Hug Your Haters: Embrace Complaints and Keep Your Customers",
    description: "Haters aren’t your problem … ignoring them is.",
    description2:
      "If it feels like there are more complaints than ever, and that you’re spending more time and money dealing with negativity and backlash, you’re right. But the rise of customer complaints is actually an enormous opportunity.",
    description3:
      "Jay teaches the critical steps necessary to retain and delight now that customer service is a spectator sport.",
    link: "",
    btn: "Learn More About Hug Your Haters",
  },
  {
    id: 5,
    title: "Virtual Emcee and Event Host",
    description:
      "Hall of Fame keynote speaker Jay Baer is also an in-demand emcee for complex, multi-day events, and has hosted events for IBM, Oracle and many other major brands for audiences as large as 15,000 attendees.",
    description2:
      "He’s an expert interviewer, a hilarious, on-the-fly host, and a master weaver together of event themes and key concepts.",
    link: "https://www.jaybaer.com/keynotes/talk-triggers/",
    btn: "Learn More About Jay As Em",
  },
];

export default SpeakingData;
