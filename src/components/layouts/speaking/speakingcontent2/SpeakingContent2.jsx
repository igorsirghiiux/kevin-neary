import "./speakingcontent2.styles.css";
import SpeakingData from "./SpeakingData";

const SpeakingContent2 = () => {
  

  return (
    <>
      {SpeakingData.map(({ id, title, description,description2,description3, link, btn }) => {
        return (
          <div className="content-container-speaking2" key={id}>
            <div className="content-wrapper-speaking2">
              <div className="article-container-speaking2">
                <div className="column-article-speaking2">
                  <div className="column1-speaking">
                    <h2>
                      <a href={link}>{title}</a>
                    </h2>
                  </div>
                  <div className="column2-speaking">
                    <p>{description}</p>
                    <p>{description2}</p>
                    <p>{description3}</p>
                    <a className="btn-primary speaking" href="#">
                      {btn}
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div className="line-speaking"></div>
          </div>
        );
      })}
    </>
  );
};

export default SpeakingContent2;
