import BooksTrigger from "../../../assets/BooksTrigger.png";
import BooksHugYour from "../../../assets/BooksHugYour.png";
import BookYoutility from "../../../assets/BookYoutility.png";
import BookUtilityRealEstate from "../../../assets/BookUtilityRealEstate.png";
import BookUtility from "../../../assets/BookUtility.png";
import BookNowRev from "../../../assets/BookNowRev.png";

const BooksData = [
  {
    img: BooksTrigger,
    title: "Talk Triggers",
    description:
      "Jay’s new book is Talk Triggers: The Complete Guide to Creating Customers With Word of Mouth. Published by Penguin Portfolio, Talk Triggers is the complete blueprint for growing a business by giving customers a memorable story to tell.",
    bookinfo: "Also the first business book with alpacas on the cover!",
    btn: "Learn More",
    BuyBtn: "Buy On Amazon",
  },
  {
    img: BooksHugYour,
    title: "Hug Your Haters",

    description:
      "Hug Your Haters: How to Embrace Complaints and Keep Your Customers. is the first book on customer service disruption, and is one of the best-selling customer service books, globally.",
    bookinfo:
      "Named one of 2016’s top 3 business books by Strategy : Business Magazine.",
    btn: "Learn More",
    BuyBtn: "Buy On Amazon",
  },
  {
    img: BookNowRev,
    title: "Now Revolution",
    description:
      "What is social media’s impact on business, and how must companies change to survive and thrive in this fast-paced, hyper-competitive world? This is the topic of The NOW Revolution: 7 Shifts to Make Your Business Faster, Smarter and More Social from Jay Baer and Amber Naslund",
    bookinfo: "",
    btn: "Learn More",
    BuyBtn: "Buy On Amazon",
  },
  {
    img: BookUtilityRealEstate,
    title: "Youtility",
    description:
      "Jay’s book Youtility: Why Smart Marketing is About Help not Hype is a New York Times and Amazon best seller. Published by Penguin Portfolio, Youtility helped redefine content marketing and digital marketing for modern business. The Youtility keynote presentation is a powerful mix of inspiration and practical, real-world examples.",
    BuyBtn: "Buy On Amazon",
  },
  {
    img: BookYoutility,
    title: "Youtility for Real Estate",
    description:
      "Co-written with digital marketing expert Erica Campbell Byrum from Homes.com and ForRent.com, Youtility for Real Estate: Why Smart Real Estate Professionals are Helping not Selling takes the core premise of Youtility – making your marketing so useful, people would pay for it – and shows how it works for the real estate business.",
    BuyBtn: "Buy On Amazon",
  },
  {
    img: BookUtility,
    title: "Youtility for Accountants",
    description:
      "Co-written with best-selling author Darren Root, Youtility for Accountants: Why Smart Accountants are Helping not Selling takes the core premise of Youtility – making your marketing so useful, people would pay for it – and gives it an accountants-only twist.",

    BuyBtn: "Buy On Amazon",
  },
];

export default BooksData;
