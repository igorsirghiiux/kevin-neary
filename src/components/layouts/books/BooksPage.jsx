import "./books.styles.css";
import BooksBg from "../../../assets/BooksBG.jpg";
import BooksData from "./BooksData";

const BooksPage = () => {
  return (
    <>
      <div className="hero-section ">
        <div className="overlay"></div>
        <img className="speaking-bg fullscreen " src={BooksBg} alt="books" />
        <div className="container">
          <div className="content">
            <h2 className="hero-title">Books by Jay</h2>
            <p className="hero-paragraph">
              Jay Baer is the New York Times best-selling author of six books
              that help you get more customers or keep those you’ve already
              earned.
            </p>
          </div>
        </div>
      </div>
      <div className="content-container-influence bookspage">
        <div className="content-wrapper-influence wrapper-books">
          <div className="article-container-influence books">
            {BooksData.map(
              ({ img, title, description, bookinfo, btn, BuyBtn }, index) => {
                const isColumnReverse = index % 2 === 0 ? "column-reverse" : "";

                console.log(index % 2 === 0);
                return (
                  <div
                    key={img}
                    className={`column-article-books ${isColumnReverse}`}
                  >
                    <div className={`column1-books `}>
                      <img src={img} alt={img} />
                    </div>

                    <div className={`column2-books `}>
                      <h4>{title}</h4>
                      <p>{description}</p>
                      {index <= 1 && (
                        <p>
                          <strong>{bookinfo}</strong>
                        </p>
                      )}

                      <div className="btn-books">
                        {index <= 1 && (
                          <a className="btn-primary" href="#">
                            {btn}
                          </a>
                        )}
                        <a
                          className="play-link"
                          href="https://youtu.be/MG1xQRJY3CE"
                        >
                          {BuyBtn}
                        </a>
                      </div>
                    </div>
                  </div>
                );
              }
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default BooksPage;
