import "./abouthersosection.styles.css";
import AboutBg from "../../../../assets/about-jay-hero.jpg";

const AboutHeroSection = () => {
  return (
    <div className="hero-section ">
      <div className="overlay"></div>
      <img className="aboutBg" src={AboutBg} alt="about" />
      <div className="container">
        <div className="content">
          <h2 className="hero-title">About Jay</h2>
          <p className="hero-paragraph">
            The world’s most inspirational customer experience, customer
            service, and marketing keynote speaker. New York Times best-selling
            author of six books. A 7th-generation entrepreneur. The founder of
            five, multi-million dollar companies. Certified tequila sommelier.
            Lover of all things plaid.
          </p>
        </div>
      </div>
    </div>
  );
};

export default AboutHeroSection;
