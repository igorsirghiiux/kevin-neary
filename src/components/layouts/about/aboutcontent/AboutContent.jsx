import "./aboutcontent.styles.css"
import MarskGroup from "../../../../assets/Mask-Group.png"

const AboutContent = () => {
  return (
    <div className="content-container-aboutcontent ">
      <div className="content-wrapper-about">
        <div className="article-container-about">
          <div className="column-article-about">
            <div className="column1-influence-about">
              <h4>Jay Baer is:</h4>
              <img src={MarskGroup} alt="Joe" />
            </div>

            <div className="column2-influence-about">
              <ul>
                <li>
                  One of fewer than 200 living members of the Professional
                  Speakers Hall of Fame
                </li>
                <li>
                  An inductee into the Word of Mouth Marketing Hall of Fame
                </li>
                <li>
                  An experienced pro, having given thousands of insightful,
                  humorous presentations world-wide with deep experience in Ag,
                  Associations, Automotive, Building/Construction, Finserv,
                  Franchise, Healthcare, Manufacturing, Pharma, Retail,
                  Technology, and Travel/Hospitality.
                </li>
                <li>A renowned business strategist</li>
                <li>A popular emcee and event host</li>
                <li>A New York Times best-selling author of six books</li>
                <li>
                  An advisor to more than 700 brands, including 40 of the
                  FORTUNE 500
                </li>
                <li>
                  The second most-popular tequila influencer in the world, who
                  can combine business growth advice with tequila education and
                  live tastings for your in-person or virtual audience
                </li>
                <li>
                  An entrepreneurial success story, having started five
                  multi-million dollar businesses from scratch
                </li>
                <li>
                  A go-to source for the press including NPR, USA Today, Time,
                  Real Simple, CBC and many more
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AboutContent