import "./aboutcontent2.styles.css";
import JayBayerImg from "../../../../assets/jay-baer.png";
import ReactPlayer from "react-player";

const AboutContent2 = () => {
  return (
    <div className="content-container-aboutcontent2">
      <div className="content-wrapper-aboutcontent2">
        <div className="article-container-aboutcontent2">
          <div className="column-article-aboutcontent2">
            <div className="column-aboutcontent2">
              <h4>Speaking Topics</h4>
              <p>
                Jay will customize his live or virtual presentation to be
                relevant and engaging to your specific audience by including
                relevant and targeted examples, often from people in the room.
              </p>
              <div className="most-popular-link">
                <a className="btn-primary " href="#">
                  Most popular Programs
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="article-container-about-story">
          <div className="column-article-influence">
            <div className="column1-about-story">
              <h4>The Jay Baer Story</h4>
              <p>Jay Baer grows businesses.</p>
              <p>
                He’s a 7th-generation entrepreneur, author of 6 best-selling
                books on customer acquisition and retention, and founder of 5
                multi-million dollar companies.
              </p>
              <p>
                Jay started his career in politics, as a direct mail specialist
                for a United States Senator.
              </p>
              <p>
                After brief forays into government service (as a spokesperson),
                and environmental services (as a marketer for a garbage
                company), he stumbled upon the Internet back when domain names
                were still FREE
              </p>
              <p>
                Seeing and seizing a business growth opportunity, Jay created
                and sold multiple digital marketing and customer experience
                companies, including Convince & Convert, one of the world’s most
                successful digital strategy advisory firms
              </p>
              <p>
                In the early 2000s, Jay conducted a tour for the publication of
                his first book. This adventure ultimately spawned his career as
                a well-known keynote speaker and event host. After more than one
                thousand presentations, he is now an inductee in the
                professional speaking hall of fame.
              </p>
              <p>
                Because Jay was a strategist and advisor long before he was a
                professional speaker, his presentations are highly customized,
                and filled with useful frameworks and practical advice.
                Organizers bring him back time after time because he constantly
                writes new material, and audiences grow their businesses as a
                result of his advice.
              </p>
              <p>
                Beyond his books, he founded the Convince & Convert blog, which
                is still visited millions of times each year, and also
                co-created the Social Pros podcast. He co-hosted the show for
                500+ episodes, and it was twice named best marketing podcast in
                the world.
              </p>
              <p>
                In addition to his own ventures, and his counsel to leading
                brands, he is an active investor and works with dozens of small
                businesses as an advisor.
              </p>
              <p>
                Growing up in Arizona and going to school in Tucson first got
                him interested in tequila. He’s now expanded his passion into a
                thriving tequila education and reviews empire, and is regarded
                as one of the world’s top tequila influencers.
              </p>
              <p>
                Jay lives in the idyllic college town of Bloomington, Indiana
                with his wife and travels from Indianapolis to speaking
                opportunities world-wide.
              </p>
            </div>
            <div className="column2-about-story">
              <img src={JayBayerImg} alt="JayBayer" />
            </div>
          </div>
        </div>
        <div className="video-press">
          <h4>Press</h4>
          <div className="video-col">
            <div className="player-press">
              <ReactPlayer
                url="https://www.youtube.com/watch?v=4HDMiEQ6Fsk&t=1s"
                muted={true}
                controls={false}
                playing={false}
                width="100%"
                height="100%"
              />
            </div>
            <div className="player-press">
              <ReactPlayer 
                url="https://www.youtube.com/watch?v=OLhnPr2-Hl0&t=19s"
                muted={true}
                controls={false}
                playing={false}
                width="100%"
                height="100%"
              />
            </div>
          </div>
          <p>
          To reach Jay Baer for a media appearance or comment, please send email
          to 
          <span><a href="https://gmail.com">jay@jaybaer.com</a></span> or text (602) 616.1895. Also, direct message on
          Twitter
          <span className="twitter-hover">
            <a href="https://twitter.com/jaybaer">@jaybaer.</a>
          </span>
        </p>
        </div>
        
      </div>
    </div>
  );
};

export default AboutContent2;
