import "./influencerprograms.styles.css";
import InfluencerBg from "../../../../assets/influencerBg.jpeg";

const InfluencerPrograms = () => {
  return (
    <div className="hero-section">
      <div className="overlay"></div>
      <img
        className="speaking-bg fullscreen"
        src={InfluencerBg}
        alt="influencere"
      />
      <div className="container">
        <div className="content">
          <h2 className="hero-title">Influencer Programs</h2>
          <p className="hero-paragraph">
            One of the world's top influencers in marketing, martech, customer
            experience, customer service, business growth, entrepreneurship, and
            beyond. Jay has created influencer programs for many of the world's
            leading brands.
          </p>
        </div>
      </div>
    </div>
  );
};

export default InfluencerPrograms;
