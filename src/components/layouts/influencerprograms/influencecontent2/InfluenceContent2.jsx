import "./influencecontent2.styles.css"

const InfluenceContent2 = () => {
  return (
    <div className="content-container-influence2" >
      <div className="content-wrapper-influence2">
        <div className="article-container-influence2">
          <div className="column-article-influence2">
            <div className="column1-influence2">
              <div className="wrap-column1-influence2">
                <p>
                Why do brands want to work with Jay for their influencer
                marketing?
              </p>
              <p>
                He knows what works, because he’s created more than one hundred
                programs.
              </p>
              <p>
                He is a lot more than “an influencer” and helps create topics,
                timelines, marketing plans, and more.
              </p>
              <p>
                Jay’s relationships in the business community are boundless, and
                he can recruit many more influencers to participate in brand
                programs.
              </p>
              <p>
                Jay has a large audience, but perhaps not the LARGEST. More
                importantly, however, he has actual INFLUENCE:
              </p>
              </div>
              
            </div>
            <div className="column2-influence2">
              <ul>
                <li>A Global Guru in Customer Service</li>
                <li>A Global Guru in Internet Marketing</li>
                <li>Top Customer Experience Speakers</li>
                <li>Top Customer Service Speakers</li>
                <li>Top B2B Marketing Influencers</li>
                <li>Top Content Marketers to Follow Right Now</li>
                <li>Top 10 Customer Experience Influencers to Watch</li>
                <li>Top Customer Experience Leaders You Must Follow</li>
                <li>Top Content Marketing Influencers to Follow in 2022</li>
                <li>Top 11 Marketing Instagram Accounts to Follow</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  );
}

export default InfluenceContent2