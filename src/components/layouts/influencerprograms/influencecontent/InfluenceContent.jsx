import "./influencecontent.styles.css";
import MarskGroup from "./../../../../assets/Mask-Group.png"

const InfluenceContent = () => {
  return (
    <div className="content-container-influence">
      <div className="content-wrapper-influence">
        <div className="article-container-influence">
          <div className="column-article-influence">
            <div className="column1-influence">
              
              <h4>
               
                  An Influencer with Expertise:
                
              </h4>
              <img src={MarskGroup} alt="Joe" />
            </div>

            <div className="column2-influence">
              <p>
                Jay Baer has worked with dozens of leading brands to boost their
                exposure and credibility through innovative B2B influencer
                marketing programs.
              </p>
              <p>
                In addition to participating as an influencer, Jay has also
                created and quarterbacked some of the largest and most effective
                B2B influencer marketing programs ever.
              </p>
              <p>
                Jay’s influencer marketing clients include: Oracle, Cisco,
                Salesforce, IBM, Comcast, SharpSpring, Emplifi, Vidyard, and
                dozens more.
              </p>
              <p>
                Jay is such a leader in this arena, that he co-authored the
                landmark study on the industry: “B2B Global Influencer
                Compensation Report.”
              </p>
              <p>
                Jay is an expert at creating winning content that gets results,
                including:
              </p>
              <ul>
                <li>Webinars</li>
                <li>Webinines (a 9-minute Webinar format Jay pioneered)</li>
                <li>First-party research reports and white papers</li>
                <li>Videos analyzing research</li>
                <li>Livestreams and live chats</li>
                <li>
                  ebooks where Jay interviews customers and influencers to
                  create a trends report
                </li>
                <li>Podcasts</li>
                <li>Live events</li>
                <li>Online game shows</li>
                <li>Short-form episodic video series</li>
              </ul>
              
            </div>
          </div>
        </div>
      </div>
      
    </div>
  );
};

export default InfluenceContent;
