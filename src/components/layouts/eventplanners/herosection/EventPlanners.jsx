import "./eventplanners.styles.css";
import EventPlannersBg from "../../../../assets/eventplannersbg.png";
import Pickplaid from "../../../../assets/pickplaid.png";

const EventPlanners = () => {
  return (
    <>
      <div className="hero-section">
        <img
          className="speaking-bg fullscreen"
          src={EventPlannersBg}
          alt="event"
        />
        <div className="container">
          <div className="content">
            <h2 className="hero-title">For Event Planners</h2>
            <p className="hero-paragraph">
              Hall of Fame Keynote Speaker Jay Baer wants to be the easiest to
              work with speaker you've ever seen. He's also one of the world's
              best (and most experienced) virtual presenters. AND, he's a
              certified tequila sommelier, and can conduct in-person or virtual
              live tequila tastings for your group. Important resources for
              event planners located below.
            </p>
            <div className="most-popular-link-event">
              <a className="btn-primary" href="#">
                DOWNLOAD INFORMATION PACKAGE
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="event-bg">
        <div className="event-container">
          <div className="event-article">
            <h4 className="event-title-card">
              Meeting planners and corporate clients love working with Hall of
              Fame speaker Jay Baer, CPAE. Why?
            </h4>
            <div className="event-columns">
              <div className="event-column1">
                <ul>
                  <li>
                    Every presentation is customized to fit the audience,
                    including examples from attendees in the room
                  </li>
                  <li>
                    Jay uses the extensive reach of his blog, podcast, email
                    list, and social media (300,000+ Twitter followers) to
                    promote your event
                  </li>
                  <li>
                    Jay’s presentations are visceral and visual. No boring
                    Powerpoint!
                  </li>
                  <li>
                    Jay’s presentations are sprinkled with humor throughout.
                    Attendees are guaranteed to leave laughing (and thinking)
                  </li>
                  <li>
                    Organized, on-time, and considerate YOU get to choose which
                    plaid suit Jay wears on stage #PickPlaid
                  </li>

                  <h5>
                    <strong>
                      Jay has the same goal for every keynote presentation and
                      emcee opportunity: to be the best, most invigorating
                      speaker you’ve worked with, ever.
                    </strong>
                  </h5>
                </ul>
              </div>
              <div className="event-column2">
                <div className="event-articles-links">
                  <h5>Event Planners’ Information Package</h5>
                  <p>
                    Background, bio, program descriptions, and event options.
                    Everything you need to know about Jay Baer, in one package.
                  </p>
                  <a href="#">Download Information Package</a>
                </div>
                <div className="event-articles-links">
                  <h5>Event Resources</h5>
                  <p>Jay Baer Introduction</p>
                  <a href="#">Download as PDF: Jay Baer Introduction</a>
                  <p>Jay Baer Biography</p>
                  <a href="#">Download as PDF: Jay Baer Biography</a>
                </div>
                <div className="event-articles-links">
                  <h5>Jay Baer Photos/Logos</h5>
                  <a href="#">Jay Baer Headshot</a>
                  <a href="#">Jay Baer Headshot Alternate</a>
                  <a href="#">Jay Baer Exterior Fun Horizontal</a>
                  <a href="#">Jay Baer Wall Lean Exterior Vertical</a>
                  <a href="#">Jay Baer Standing Vertical</a>
                  <a href="#">Jay Baer Seated Black & White</a>
                  <a href="#">Jay Baer Suit Jacket Reveal</a>
                  <a href="#">Jay Baer Logo Square</a>
                  <a href="#">Jay Baer Logo Horizontal</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="wrap-article-content event-content">
        <div className="wrap-article">
          <div className="event-contact-jay ">
            <h3>
              Check Jay's <span className="orange-text">Availability</span> for
              Your Event
            </h3>
            <a className="btn-primary" href="#">
              CONTACT JAY BAER
            </a>
          </div>
          <div className="article-container">
            <div className="column-article">
              <div className="column1-content2">
                <h2>Pick Jay Baer and #PickPlaid</h2>
                <p>
                  In addition to being the world’s most inspirational marketing
                  and customer service keynote speaker, Jay Baer is also a
                  unique visual presence on stage. Audiences are amazed by his
                  array of very bold, very plaid suits. Plus, YOU get to pick
                  the suit – even for virtual events! For each event, meeting
                  planners are provided a link to a special, interactive tool
                  where they select the color and pattern of suit Jay wears on
                  stage. Want to match your corporate color? You can do it in
                  seconds with the exclusive Dress Jay Baer app, just one of the
                  many reasons meeting planners and company executives
                  #PickPlaid.
                </p>
                <div className="most-popular-link-event">
                  <a className="btn-primary" href="#">
                    SEE ALL POPULAR PROGRAMS
                  </a>
                </div>
              </div>

              <div className="column2-content2">
                <img src={Pickplaid} alt="Pickplaid" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default EventPlanners;
