const SlideData = [
  {
    url: "https://www.youtube.com/watch?v=zGvtwAjzutQ&t=2s",
    title: "The Time to Win (Based on Proprietary New Research)",
    paragraph: "Grow Your Business by Exceeding Your Customers’ Need for Speed",
    btn: "Learn More and See Sample Video",
  },
  {
    url: "https://www.youtube.com/watch?v=Y7fJp-ZFVT4",
    title: "Coveted Customer Experience ",
    paragraph:
      "Build Your Business by Focusing on the 3 Things Customers Care About More Than Ever",
    btn: "Learn More and See Sample Video",
  },
  {
    url: "https://www.youtube.com/watch?v=br4zYtAqlrU",
    title: "Talk Triggers ",
    paragraph:
      "Turn Your Customers into Your Best Sales and Marketing Advantage",
    btn: "Learn More and See Sample Video",
  },
  {
    url: "https://www.youtube.com/watch?v=FJrH4x3_zQ0",
    title: "Virtual Emcee and Event Host ",
    paragraph: "Jay Brings the Energy to Your Event",
    btn: "Learn More and See Sample Video",
  },
  {
    url: "https://www.youtube.com/watch?v=zGvtwAjzutQ&t=2s",
    title: "Hug Your Haters ",
    paragraph: "Grow Your Business by Exceeding Your Customers’ Need for Speed",
    btn: "Learn More and See Sample Video",
  },
];

export default SlideData;
