import React from "react";
import "./slidecard.styles.css";
import ReactPlayer from "react-player";
import SlideData from "./slideData";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/pagination";
import "swiper/css/navigation";

const SlideCards = () => {
  return (
    <div className="bg-slide">
      <div className="glide-slide-container"></div>
      <Swiper
        style={{
          "--swiper-navigation-size": "30px",
        }}
        slidesPerView={1}
        breakpoints={{
          1500: {
            slidesPerView: 3,
          },
          800: {
            slidesPerView: 2,
          },
          500: {
            slidesPerView: 1,
          },
        }}
        spaceBetween={60}
        loop={true}
        loopedSlides={2}
        navigation={true}
        modules={[Navigation]}
        className="mySwiper"
      >
        {SlideData.map(({ url, title, paragraph, btn }, index) => (
          <SwiperSlide className="top-position" key={`slide-${index}`}>
            <div className="player-wrapper">
              <ReactPlayer
                url={url}
                muted={true}
                controls={false}
                playing={false}
                width="100%"
                height="100%"
              />
            </div>

            <div className="slide-content">
              <h5>{title}</h5>
              <p>{paragraph}</p>
              <a
                className="reveal-link"
                href="https://jaybaer.com/keynotes/hug-your-haters/"
              >
                <span>{btn}</span>
              </a>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default SlideCards;
