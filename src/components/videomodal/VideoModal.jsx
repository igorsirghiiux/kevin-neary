import { useState } from "react";
import "./videomodal.style.css";
import ReactPlayer from "react-player";
import VideoImgBg from "../../assets/imgvideobg.jpg";

const VideoModal = () => {
  const [showVideo, setShowVideo] = useState(false);
  const [closeVideo, setCloseVideo] = useState(false);
  return (
    <div className="video-container" onClick={() => setShowVideo(!showVideo)}>
      <div className="opacity-video"></div>
      <img src={VideoImgBg} alt="video" />
      <div className="video-data">
        <div className="video-play"></div>
        <div className="video-heading">
          <h2>SEE JAY IN ACTION!</h2>
          <p>You’ve never seen one like This!</p>
        </div>
        {showVideo && (
          <div className="video-modal">
            <div className="modal-content">
              <div
                className="modal-close"
                onClick={() => setCloseVideo(!closeVideo)}
              >
                X
              </div>
              <div className="iframe-video">
                <ReactPlayer
                  url="https://www.youtube.com/watch?v=MG1xQRJY3CE"
                  muted={true}
                  controls={true}
                  playing={true}
                  width="1300px"
                  height="750px"
                />
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default VideoModal;
