import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import "./navbar.styles.css";
import Logo from "../../assets/LogoKevin.svg";
import Hamburger from "hamburger-react";
import OrcawiseLogo from "../../assets/orcawise.svg"

const Navbar = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isBlack, setIsBlack] = useState(false);
  const [screenWidth, setScreenWidth] = useState(window.innerWidth);
  const location = useLocation();

  const handleMenuToggle = () => {
    setIsOpen(!isOpen);
    setIsBlack(!isBlack);
  };

  useEffect(() => {
    const handleResize = () => {
      setScreenWidth(window.innerWidth);
      if (window.innerWidth >= 1150 && isOpen) {
        setIsOpen(false);
        setIsBlack(false);
      }
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [isOpen]);

  return (
    <nav className={`nav-container ${isBlack ? "black-bg" : ""}`}>
      <div className="wrap">
        <Link to="/" className="custom-logo-link" href="#">
          <img
            src={Logo}
            alt="Customer Experience and Marketing Keynote Speaker and Emcee Jay Baer"
            className="custom-logo-link"
          />
        </Link>

        <ul
          className={isOpen ? "nav-links-mobile" : "nav-links-menu"}
          onClick={() => setIsOpen(false)}
        >
          <li>
            <Link
              to="/"
              className={`links ${location.pathname === "/" ? "active" : ""}`}
            >
              Newsletter
            </Link>
          </li>
          <li>
            <Link
              to="/speaking"
              className={`links ${
                location.pathname === "/speaking" ? "active" : ""
              }`}
            >
              Speaking
            </Link>
          </li>
          <li>
            <Link
              to="/influencer-programs"
              className={`links ${
                location.pathname === "/influencer-programs" ? "active" : ""
              }`}
            >
              Influencer Programs
            </Link>
          </li>
          <li>
            <Link
              to="/about"
              className={`links ${
                location.pathname === "/about" ? "active" : ""
              }`}
            >
              About
            </Link>
          </li>
          <li>
            <Link
              to="/books"
              className={`links ${
                location.pathname === "/books" ? "active" : ""
              }`}
            >
              Books
            </Link>
          </li>

          <li>
            <Link
              to="/event-planner"
              className={`links ${
                location.pathname === "/event-planner" ? "active" : ""
              }`}
            >
              Event Planners
            </Link>
          </li>
          <li>
            <Link
              to="/contact"
              className={`links contact-button ${
                location.pathname === "/contact" ? "active" : ""
              }`}
            >
              Contact
            </Link>
          </li>
        </ul>

        {/* Only show the Hamburger component if the screen width is less than 1150px */}
        {screenWidth < 1150 && (
          <Hamburger
            color="#eb6028"
            toggled={isOpen}
            toggle={handleMenuToggle}
          />
        )}
      </div>
    </nav>
  );
};

export default Navbar;
