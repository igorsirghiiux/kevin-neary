import React from "react";
import "./footer.styles.css";
import Logo from "../../assets/Logo.png";

const Footer = () => {
  return (
    <div className="wrap-article-footer">
      <div className="wrap-article">
        <div className="footer-widgets">
          <div className="footer-column1">
            <img src={Logo} alt="logo" />
          </div>
          <div className="footer-column2">
            <ul className="footer-menu">
              <li>
                <a href="#">Keynote Speaker</a>
              </li>
              <li>
                <a href="#">Customer Experience Keynote Speaker</a>
              </li>
              <li>
                <a href="#">Customer Service Keynote Speaker</a>
              </li>
              <li>
                <a href="#">Marketing Keynote Speaker</a>
              </li>
              <li>
                <a href="#">Word of Mouth Speaker</a>
              </li>
              <li>
                <a href="#">Blog and Podcasts</a>
              </li>
              <li>
                <a href="#">Privacy Policy</a>
              </li>
              <li>
                <a href="#">
                  CLICK TO CONTACT OR CALL MICHELLE JOYCE (704) 965-2339
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
